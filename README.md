# airflow_restore_training

## Description

### Background
Airflow 可用來建立各式的流程與 Pipeline。其中 ML pipeline 是一個常見的應用方式。在 ML pipeline 的應用中，包括了模型訓練的任務。但 Airflow 並未對這類型的任務進行保護，僅提供「重啟任務」的恢復選項。這意味著，先前訓練的模型狀態皆不保存，使得需要任務需要從頭執行。

### Objective
訓練任務最長的執行時間可達數月或數日，而以檢查點的方式保存狀態，能使訓練在發生中斷事件時，有效地還原任務，能在「重啟任務」的恢復過程中，自動帶回任務狀態，達到恢復狀態之目的。

引入兩個 Hook Function ，就能夠保存與恢復任務狀態，並保持計算過程的一致性。


## 系統環境
* Kubernete: v1.28.5
* Apache Airflow:v2.8.2
    * git-sync:v4.2.2
* Minio Object Repository:  RELEASE.2024-01-18T22-51-28Z
    * Runtime: go1.21.6 linux/amd64
* Pytorch
* NFS Server

## 整體系統架構圖
![](./public/overall_system_architecture.png)

### 論文方法之系統架構圖
![](./public/system_architecture.png)
### 實驗架構圖
![](./public/experiment_system_architecture.png)

## 檔案目錄
- [API_Service](./API_Server/): 兩個子功能
    - 【func】 killPodWithPodName_API : 從 API Server 向 KubeAPI 請求刪除此 Pod
    - 【func】 getAirflowTaskID_API：  從 API Server 向 KubeAPI 請求 Label 並回傳 Pod 的 Airflow Task ID
- [training task](./trainingTask/)
    - pytorch: 訓練任務的檔案位置
    - DockerFile：封裝訓練任務為 Container 的 Docker file
    - [readme](./trainingTask/readme.md)
- [airflow_dag](./airflow_dag/): airflow 運行 Pytorch Training Task 的 DAG File
    - kubernetes_config: 以 K8S Pod 之 Yaml 檔案生成 Pytorch Training Task，與 Airflow Task 可以相互對應
- [system](./system/): 系統安裝的相關配置檔
    - airflow: airflow Pod 安裝配置檔案，安裝、解除安裝之腳本
    - minio: Minio Pod 安裝配置檔案，安裝、解除安裝之腳本




# Getting started
## 安裝 Kubernetes
* [【Hackmd】Kubernetes 安裝筆記](https://hackmd.io/Gusb6C6BRn-5uP2DLRzBaQ)
* [如何在 Ubuntu 20.04 上安裝 Kubernetes](https://phoenixnap.com/kb/install-kubernetes-on-ubuntu)
* [【Kubernetes】install-kubeadm](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/)
* [【Kubernetes】Container Runtimes](https://kubernetes.io/docs/setup/production-environment/container-runtimes/)
    * Ubuntu 22.04： cgroup v2， 需要將Containerd 配置檔案更新 ==SystemdCgroup = true==


## 安裝 Contianer Runtime
* [【NVIDIA】Installing the NVIDIA Container Toolkit](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/latest/install-guide.html)
### 切記要先綁定 Drvier 版本！！！！
```
sudo apt-mark hold nvidia-driver-550
```
https://blog.csdn.net/weixin_44649780/article/details/130235654
```
nvcc -V
```
### 參考資料
* [【Nvidia】CUDA Toolkit 12.4 Update 1 Downloads](https://developer.nvidia.com/cuda-downloads?target_os=Linux&target_arch=x86_64&Distribution=Ubuntu&target_version=22.04&target_type=deb_network)
* [NVIDIA驱动失效简单解决方案：NVIDIA-SMI has failed because it couldn‘t communicate with the NVIDIA driver.](https://blog.csdn.net/wjinjie/article/details/108997692)
* [nvcc -V 与 nvidia-smi显示的cuda版本不一致问题原因及解决途径](https://blog.csdn.net/onepunch_k/article/details/123334444)


## 安裝 Airflow
* [Helm Chart for Apache Airflow](https://airflow.apache.org/docs/helm-chart/stable/index.html)
* [k8s（helm） 部署airflow(详细可实施）](https://blog.csdn.net/weixin_43921297/article/details/132741121)
* [【Hackmd】Airflow 安裝筆記 ](https://hackmd.io/KK6Q4SWKR32RZgJ9eH-w_g)
### gitlab 新增 sshkey
```==
# 創建您的 ssh 密鑰
$ ssh-keygen -t rsa -b 4096 -C "your_email@example.com"
```
User Settings\ SSH Keys\ 

```
kubectl create secret generic airflow-git-ssh-secret \
  --from-file=gitSshKey=/home/renjie/.ssh/id_rsa \
  --from-file=known_hosts=/home/renjie/.ssh/known_hosts \
  --from-file=id_ed25519.pub=/home/renjie/.ssh/id_rsa.pub \
  -n airflow
```
* [Setting up Apache Airflow on Kubernetes with GitSync](https://blog.devgenius.io/setting-up-apache-airflow-on-kubernetes-with-gitsync-beaac2e397f3)
* [为GitLab帐号添加SSH keys并连接GitLab](https://blog.csdn.net/xyzchenxiaolin/article/details/51852333)
* [使用 Git-Sync sidecar 從私有 GitHub 儲存庫掛載 DAG](https://airflow.apache.org/docs/helm-chart/stable/manage-dags-files.html#mounting-dags-from-a-private-github-repo-using-git-sync-sidecar)

### 安裝
```
bash ./system/airflow/airflow_install.sh
```

### Web Port-fowarding
#### airflow-webserver
```
kubectl port-forward svc/airflow-webserver 8080:8080 --namespace airflow --address='0.0.0.0'
```
#### airflow-postgresql
```
kubectl port-forward svc/airflow-postgresql 5432:5432 --namespace airflow --address='0.0.0.0'
```

### 解除安裝
```
bash ./system/airflow/airflow_uninstall.sh
```



## 安裝 Minio
* [【Hackmd】Minio 安裝筆記](https://hackmd.io/Gu1mJjf-Rryo2UiLlo9aBw)
```
bash ./system/airflow/minio_install_objectStorage.sh
```

```
bash ./system/airflow/minio_uninstall_objectStorage.sh
```


## NFS Server
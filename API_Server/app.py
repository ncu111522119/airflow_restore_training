from flask import Flask, request, jsonify
import datetime
import time
from kubernetes import client, config


## get Airflow Task ID with PodIP
# Configs can be set in Configuration class directly or using helper utility
config.load_kube_config()

"""
Kill Kubenetes Pod
"""
def killPodWithPodName(podName: str, pod_namespace:str, dag_id:str):
    v1 = client.CoreV1Api()
    if dag_id == "dagid": # at Local
        return None
    else: # in kubernetes cluster
        print("killed")
        v1.delete_namespaced_pod(podName, pod_namespace)


"""
Get ID of Airflow Task
從 Kubernetes API 
"""
def getAirflowTaskID(PodIP):
    v1 = client.CoreV1Api()
    ret = v1.list_pod_for_all_namespaces(watch=False)
    for i in ret.items:
        if PodIP == i.status.pod_ip:
            print("%s\t%s\t%s\t%s" % (i.status.pod_ip, i.metadata.namespace, i.metadata.name, i.metadata.labels))
            if "run_id"  in i.metadata.labels:
                return {"pod_ip":i.status.pod_ip,
                        "pod_name": i.metadata.name,
                        "pod_namespace": i.metadata.namespace,
                        "run_id": i.metadata.labels["run_id"], 
                        "task_id": i.metadata.labels["task_id"], 
                        "dag_id": i.metadata.labels["dag_id"],
                        "try_number": i.metadata.labels['try_number']
                        }
            else:
                break
    
    return None


app = Flask(__name__)
 
 
@app.route('/', methods=['POST'])
def num_for():
    data = request.json
    number = int(data.get('num'))
    for i in range(number):
        time.sleep(1)
        print(i)
    return jsonify(result='OK', time=datetime.datetime.today())
 
@app.route('/getAirflowTaskID', methods=['POST'])
def getAirflowTaskID_API():
    data = request.json
    PodIP = data.get('PodIP')
    result = getAirflowTaskID(PodIP)
    result = result if result != None else ""
    return jsonify(result=result, time=datetime.datetime.today())

@app.route('/killPodWithPodName', methods=['POST'])
def killPodWithPodName_API():
    data = request.json
    PodIP = data.get('PodIP')
    result = getAirflowTaskID(PodIP)
    killPodWithPodName(podName=result["pod_name"], pod_namespace=result["pod_namespace"], dag_id = result["dag_id"])
    result = result if result != None else ""
    return jsonify(result=result, time=datetime.datetime.today())

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port=8888)
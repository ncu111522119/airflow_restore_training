# PV 與 PVC 宣告
宣告 PV 與 PVC 讓 Training Dataset 的 NFS  供 Pod 使用
## PV
```
---
apiVersion: v1
kind: PersistentVolume
metadata:
  name: imagenet-pv-nfs
  namespace: imagenet-dataset
  labels:
    type: training_dataset
spec:
  capacity:
    storage: 400Gi
  volumeMode: Filesystem
  accessModes:
    - ReadWriteMany
  mountOptions:
    - hard
    - nfsvers=4.1
  nfs:
    path: /dataset/mnt
    server: 10.52.52.25
```
## PVC
```
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: imagenet-pvc-nfs
  namespace: imagenet-dataset
spec:
  accessModes:
    - ReadWriteMany
  volumeMode: Filesystem
  resources:
    requests:
      storage: 400Gi
```


# Pod 封裝任務之完整程式碼：Kubernetes 與 Airflow 程式碼比對
## Kuberntes pod yaml: training task
```
apiVersion: v1
kind: Pod
metadata:
  name: train-resnet-1
  namespace: imagenet-dataset
  labels:
    dag_id: lab6-3_ResnetTraining_PodOperator
    run_id: manual__2024
    task_id: training_task
spec:
  containers:
    - name: train-resnet-imagenet-1
      command: ["python3", "-u", "main.py", "--epoch", "35", "--dataset", "/dataset" ]
      env:
      - name: APIServer_url
        value: "http://10.52.52.100:8888/getAirflowTaskID"
      image: ncu111522119/train_resnet:latest
      volumeMounts:
        - name: imagenet-vol
          mountPath: /dataset
        - name: shm-memory
          mountPath: /dev/shm
  volumes:
    - name: imagenet-vol
      persistentVolumeClaim:
        claimName: imagenet-pvc-nfs
    - name: shm-memory
      emptyDir:
        medium: Memory
        sizeLimit: 8Gi
```

## Airflow Task： training task
```
# 宣告 volumes
imagenet_pvc_volume = k8s.V1Volume(
        name="imagenet-vol",
        persistent_volume_claim=k8s.V1PersistentVolumeClaimVolumeSource(claim_name="imagenet-pvc-nfs"),
    )

shm_memory_volume = k8s.V1Volume(
        name="shm-memory",
        empty_dir=k8s.V1EmptyDirVolumeSource(medium="Memory", size_limit="8Gi"),
    )

# volume mount 在 container 內部使用，並指定在 Container 內部的路徑
imagenet_volume_mounts=k8s.V1VolumeMount(
    mount_path="/dataset", name="imagenet-vol", sub_path=None, read_only=False
)

shm_memory_volume_mounts=k8s.V1VolumeMount(
    mount_path="/dev/shm", name="shm-memory", sub_path=None, read_only=False
)


# 定義 DAG
with DAG(
    dag_id="lab_ckpt_complete_noCKPT",
    schedule=None,
    catchup=False,
    # start_date=datetime(2024, 1, 31),
    tags=["paper_Lab"],
    default_args={
        "retries": 5,
        "retry_delay": timedelta(minutes=1),
    }

) as dag:

    # 定義 Task ，並以 Pod、Container 封裝任務
    ckpt_complete = KubernetesPodOperator(
        task_id="ckpt_complete",
        name="ckpt_complete",
        image="ncu111522119/train_resnet:seed_v2",
        namespace="imagenet-dataset",
        node_selector={"host": "workergpu"}, #"resource": "gpu"
        env_vars={'getAirflowTaskID_url': 'http://10.52.52.100:8888/getAirflowTaskID', 'killPod_url': 'http://10.52.52.100:8888/killPodWithPodName'},
        volume_mounts=[imagenet_volume_mounts, shm_memory_volume_mounts], # container 內部使用，以及預計使用的路徑
        volumes=[imagenet_pvc_volume, shm_memory_volume], # 定義 Pod 之 Volumes
        cmds= ["python3", "main.py", "--epoch", "31", "--dataset", "/dataset", "--setKill", "False", "--enableCKPT" , "True", "--enableSeed", "True" ] # 每13小時 kill 一次
    )

  
    ckpt_complete
```

## 宣告 volumes
事先定義 Volumes 與 volume名稱，以供後續 Container 層級使用。
Volume 分成兩種 ephemeral volumes、persistent volumes。可以是暫時性的，也可以是持久性儲存。
在 Kubernetes 中可以支援多類型的 volumes
* configMap、secrete
    * key-value 的保存形式
    * read-only
* 【ephemeral】emptyDir
* hostPath
    * 當 Pod 不同機器，可能導致資料不一致
* persistentVolumeClaim
    * 透過 PV 綁定

### 參考資料
1. [【ithelp】 Day 17 Storage](https://ithelp.ithome.com.tw/articles/10332040?sc=rss.iron)

### 本案例
* 設定兩個Volume：
    * imagenet-vol
    * shm-memory
* imagenet-vol
    * NFS Server
    * 存放 ImageNet-1K 之資料集
    * 與 imagenet-pvc-nfs PVC 嫁接
* shm-memory
    * 擴充 Container 的 Memory 大小，讓訓練能夠執行，避免 OOM
    * 當Container 關閉後，會一併消失

### Kubernetes Pod 寫法
```
spec:
    volumes:
        - name: imagenet-vol
          persistentVolumeClaim:
            claimName: imagenet-pvc-nfs
        - name: shm-memory
          emptyDir:
            medium: Memory
            sizeLimit: 8Gi
```
### Airflow Pod Operater
```
imagenet_pvc_volume = k8s.V1Volume(
        name="imagenet-vol",
        persistent_volume_claim=k8s.V1PersistentVolumeClaimVolumeSource(claim_name="imagenet-pvc-nfs"),
    )

shm_memory_volume = k8s.V1Volume(
        name="shm-memory",
        empty_dir=k8s.V1EmptyDirVolumeSource(medium="Memory", size_limit="8Gi"),
    )
```


## volume mount
### Kubernetes Pod 寫法
```
spec:
  containers:
    - name: train-resnet-imagenet-1
                    .
                    .
                    .
      volumeMounts:
        - name: imagenet-vol
          mountPath: /dataset
        - name: shm-memory
          mountPath: /dev/shm
```
### Airflow Pod Operater
```
# volume mount 在 container 內部使用，並指定在 Container 內部的路徑
imagenet_volume_mounts=k8s.V1VolumeMount(
    mount_path="/dataset", name="imagenet-vol", 
    sub_path=None, read_only=False
)

shm_memory_volume_mounts=k8s.V1VolumeMount(
    mount_path="/dev/shm", name="shm-memory", 
    sub_path=None, read_only=False
)
```





'''
實驗二：只中斷一次，第13小時中斷任務
=> 情境 2-2 : 測試 【使用】 CKPT Hook Func
=> 目的：測試使用後的成效
'''

from __future__ import annotations

import os
from datetime import datetime, timedelta

from kubernetes.client import models as k8s

from airflow import DAG
from airflow.providers.cncf.kubernetes.operators.pod import KubernetesPodOperator

imagenet_pvc_volume = k8s.V1Volume(
        name="imagenet-vol",
        persistent_volume_claim=k8s.V1PersistentVolumeClaimVolumeSource(claim_name="imagenet-pvc-nfs"),
    )

shm_memory_volume = k8s.V1Volume(
        name="shm-memory",
        empty_dir=k8s.V1EmptyDirVolumeSource(medium="Memory", size_limit="8Gi"),
    )

imagenet_volume_mounts=k8s.V1VolumeMount(
    mount_path="/dataset", name="imagenet-vol", sub_path=None, read_only=False
)

shm_memory_volume_mounts=k8s.V1VolumeMount(
    mount_path="/dev/shm", name="shm-memory", sub_path=None, read_only=False
)



with DAG(
    dag_id="lab_ckpt_complete_noCKPT",
    schedule=None,
    catchup=False,
    # start_date=datetime(2024, 1, 31),
    tags=["paper_Lab"],
    default_args={
        "retries": 5,
        "retry_delay": timedelta(minutes=1),
    }

) as dag:
   

    # ckpt_complete = KubernetesPodOperator(
    #     task_id="ckpt_complete",
    #     name="ckpt_complete",
    #     image="ncu111522119/train_resnet:latest",
    #     namespace="imagenet-dataset",
    #     node_selector={"host": "workergpu2"}, #"resource": "gpu"
    #     env_vars={'getAirflowTaskID_url': 'http://10.52.52.100:8888/getAirflowTaskID', 'killPod_url': 'http://10.52.52.100:8888/killPodWithPodName'},
    #     volume_mounts=[imagenet_volume_mounts, shm_memory_volume_mounts],
    #     volumes=[imagenet_pvc_volume, shm_memory_volume],
    #     cmds= ["python3", "main.py", "--epoch", "31", "--dataset", "/dataset", "--setKill", "False", "--enableCKPT" , "False", "--enableSeed", "False" ] # 每13小時 kill 一次
    # )
    
    
    ckpt_complete = KubernetesPodOperator(
        task_id="ckpt_complete",
        name="ckpt_complete",
        image="ncu111522119/train_resnet:seed_v2",
        namespace="imagenet-dataset",
        node_selector={"host": "workergpu"}, #"resource": "gpu"
        env_vars={'getAirflowTaskID_url': 'http://10.52.52.100:8888/getAirflowTaskID', 'killPod_url': 'http://10.52.52.100:8888/killPodWithPodName'},
        volume_mounts=[imagenet_volume_mounts, shm_memory_volume_mounts],
        volumes=[imagenet_pvc_volume, shm_memory_volume],
        cmds= ["python3", "main.py", "--epoch", "31", "--dataset", "/dataset", "--setKill", "False", "--enableCKPT" , "True", "--enableSeed", "True" ] # 每13小時 kill 一次
    )

    


    ckpt_complete_1 = KubernetesPodOperator(
        task_id="ckpt_complete_1",
        name="ckpt_complete_1",
        image="ncu111522119/train_resnet:seed_v2",
        namespace="imagenet-dataset",
        node_selector={"host": "workergpu"}, #"resource": "gpu"
        env_vars={'getAirflowTaskID_url': 'http://10.52.52.100:8888/getAirflowTaskID', 'killPod_url': 'http://10.52.52.100:8888/killPodWithPodName'},
        volume_mounts=[imagenet_volume_mounts, shm_memory_volume_mounts],
        volumes=[imagenet_pvc_volume, shm_memory_volume],
        cmds= ["python3", "main.py", "--epoch", "31", "--dataset", "/dataset", "--setKill", "False", "--enableCKPT" , "True", "--enableSeed", "True" ] # 每13小時 kill 一次
    )


    ckpt_complete_2 = KubernetesPodOperator(
        task_id="ckpt_complete_2",
        name="ckpt_complete_2",
        image="ncu111522119/train_resnet:latest",
        namespace="imagenet-dataset",
        node_selector={"host": "workergpu2"}, #"resource": "gpu"
        env_vars={'getAirflowTaskID_url': 'http://10.52.52.100:8888/getAirflowTaskID', 'killPod_url': 'http://10.52.52.100:8888/killPodWithPodName'},
        volume_mounts=[imagenet_volume_mounts, shm_memory_volume_mounts],
        volumes=[imagenet_pvc_volume, shm_memory_volume],
        cmds= ["python3", "main.py", "--epoch", "31", "--dataset", "/dataset", "--setKill", "False", "--enableCKPT" , "False", "--enableSeed", "False" ] # 每13小時 kill 一次
    )

  
    ckpt_complete >> ckpt_complete_1 >> ckpt_complete_2
echo ==解除 airflow 安裝==
helm delete airflow --namespace airflow

# echo $'\n' ==刪除 airflow namespace==
kubectl delete namespace airflow

# Airflow PV delete
echo $'\n' ==刪除 airflow 使用的 PV==

##
list=($(kubectl get pv| awk '{print $1}'))

## 取得目前陣列的個數，#: 代表個數
len=${#list[@]} 


for ((i=1; i < len; i++)); do
    echo $ kubectl delete pv ${list[$i]}
    kubectl delete pv ${list[$i]}
done

echo $'\n'====刪除目錄====
for ((i=1; i < 6; i++)); do
    echo $  rm -r ./mnt/pv$i
    rm -r ./mnt/pv$i
done

## 執行方式: $ bash ./airflow_uninstall.sh
# bash airflow_uninstall.sh

echo $'\n'===建立目錄===
for ((i=1; i < 6; i++)); do
    echo $  mkdir ./mnt/pv$i
    mkdir ./mnt/pv$i
done

echo ===設定 airflow pv===
kubectl create namespace airflow

echo \n ===設定 airflow pv===
kubectl apply -f setAirflowVolume.yaml

echo $'\n' ===建立 git secret，提供 airflow 使用===
kubectl apply -f git-credentials.yaml

echo $'\n' ===建立 git secret，提供 airflow 使用 git ssh===
kubectl create secret generic airflow-git-ssh-secret \
  --from-file=gitSshKey=/home/renjie/.ssh/id_rsa \
  --from-file=known_hosts=/home/renjie/.ssh/known_hosts \
  --from-file=id_ed25519.pub=/home/renjie/.ssh/id_rsa.pub \
  -n airflow

echo $'\n' ===安裝 airflow ，在 K8S cluster===

helm repo add apache-airflow https://airflow.apache.org
helm repo update

helm upgrade --install airflow apache-airflow/airflow --namespace airflow --create-namespace --debug \
  --set postgresql.volumePermissions.enabled=true \
  --set worker.volumePermissions.enabled=true \
  --set workers.persistence.enabled=false \
  --set executor=KubernetesExecutor \
  --set images.migrationsWaitTimeout=120 \
  --set airflowVersion=2.7.0 \
  --set dags.gitSync.enabled=true\
  --set dags.gitSync.branch=main \
  --set dags.gitSync.credentialsSecret=git-credentials \
  --set dags.gitSync.sshKeySecret=airflow-git-ssh-secret \
  --set dags.gitSync.repo=git@gitlab.com:ncu111522119/test_airflow_dag.git \
  --set dag_folder=/opt/airflow/dags/repo \
  --set images.gitSync.tag=v4.2.2 \
  --set logs.persistence.enabled=true \
  --set logs.persistence.existingClaim=testlog-volume \
  --set worker.volumePermissions.enabled=true \
  --set workers.persistence.enabled=false \
  --set triggerer.securityContext.runAsUser=0 \
  --set triggerer.securityContext.fsGroup=0 \
  --set dagProcessor.securityContext.runAsUser=0 \
  --set dagProcessor.securityContext.fsGroup=0 \
  --set scheduler.securityContext.runAsUser=0 \
  --set scheduler.securityContext.fsGroup=0 \
  --set workers.securityContext.runAsUser=0 \
  --set workers.securityContext.fsGroup=0 \
  --set webserver.securityContext.runAsUser=0 \
  --set webserver.securityContext.fsGroup=0 \
  -f ~/airflow/k8s/value.yaml
  # --set scheduler.waitForMigrations.enabled=false \
  # --set webserver.waitForMigrations.enabled=false \
  # --set workers.waitForMigrations.enabled=false \
  # --set triggerer.waitForMigrations.enabled=false \
  # --set dagProcessor.waitForMigrations.enabled=false \
# helm upgrade --install airflow apache-airflow/airflow --namespace airflow --debug \
#   --set postgresql.volumePermissions.enabled=true \
#   --set worker.volumePermissions.enabled=true \
#   --set workers.persistence.enabled=false \
#   --set executor=KubernetesExecutor \
#   --set images.migrationsWaitTimeout=120 \
#   --set airflowVersion=2.7.0 \
#   --set triggerer.securityContext.runAsUser=0 \
#   --set triggerer.securityContext.fsGroup=0 \
#   --set dags.gitSync.enabled=true\
#   --set dags.gitSync.branch=main \
#   --set dags.gitSync.credentialsSecret=git-credentials \
#   --set dags.gitSync.sshKeySecret=airflow-git-ssh-secret \
#   --set dags.gitSync.repo=git@gitlab.com:ncu111522119/test_airflow_dag.git \
#   --set dag_folder=/opt/airflow/dags/repo \
#   --set images.gitSync.tag=v3.6.9 \
#   --set logs.persistence.enabled=true \
#   --set logs.persistence.existingClaim=testlog-volume \
#   --set dagProcessor.securityContext.runAsUser=0 \
#   --set dagProcessor.securityContext.fsGroup=0 \
#   --set scheduler.securityContext.runAsUser=0 \
#   --set scheduler.securityContext.fsGroup=0 \
#   --set workers.securityContext.runAsUser=0 \
#   --set workers.securityContext.fsGroup=0 \
#   --set webserver.securityContext.runAsUser=0 \
#   --set webserver.securityContext.fsGroup=0 -f ~/airflow/k8s/value.yaml
################
#  --set logs.persistence.enabled=true \--set airflowVersion=2.7.1 \
#  --set logs.persistence.existingClaim=testlog-volume \
#  --set dags.gitSync.repo=https://gitlab.com/ncu111522119/test_airflow_dag.git
  # 


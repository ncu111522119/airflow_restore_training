# Training Task
## Description
* 訓練任務，使用 ResNet18， ImageNet-1K的資料集
* 並將程式碼封裝為 Container

#  ImageNet-1K的資料集
* [【Kaggle】ImageNet Object Localization Challenge](https://www.kaggle.com/c/imagenet-object-localization-challenge)

## NFS 安裝
供 K8S Container 共用 Volume
* [在 Oracle Linux 上建立 NFS 伺服器](https://docs.oracle.com/zh-tw/learn/create_nfs_linux/#more-learning-resources)


# 目錄結構
- /pytorch
    - [/myCkpt](./pytorch/myCkpt/)：moduel，論文的實作方法「Checkpoint Hook Function」
    - [/myKillProcess](./pytorch/myKillProcess/)：moduel，論文中實驗第二節的 Kill Pod 的計時器。
    - [/MyOutput](./pytorch/MyOutput)：moduel，論文中實驗數據的相關模組
    - [main.py](./pytorch/main.py)
    - [runMain.sh](./pytorch/runMain.sh): 在本地端執行 main.py 的腳本檔案
- Dockerfile: 將 Pytorch 的目錄封裝為 Container
- buildAndPush.sh: 封裝 Container 的腳本檔案

# main.py
使用自 Pytorch Example 程式，本論文使用 Pytroch ResNet 18。
* [【Pytorch】Docs > PyTorch Examples](https://pytorch.org/examples/)
    * [【Pytorch github】Training ImageNet Classifiers](https://github.com/pytorch/examples/tree/main/imagenet)

## 啟動參數
- --epochs：預計執行訓練的總次數
- --dataset：預計讀取資料集的檔案路徑
- --output_path：預計最後輸出檔案的位置，主要用於實驗數據
- --setKill：設定是否啟動「自殺計時器」
    - --setTimeToKill：自殺計時器預計「在甚麼時候啟動」，(秒)為單位
- --enableCKPT：啟動 Checkpoint Hook Function
- --enableSeed：使用 Checkpoint Hook Function 提供的「一致性 Random Seed 功能」


# moduel
## myCkpt

論文中的 Propose Method，Hook Function 的使用方法

### 引入 Checkpoint Hook Function Moduel
```
from myCkpt import checkpoint as thisCkpt
```
### Random Seed 初始化
```
thisCkpt.loadCheckpoint(args.enableSeed)
```
### Hook Function 恢復點
```
## 取得 Checkpoint
ckpt_bool, ckptJson = thisCkpt.loadCheckpoint(args.enableSeed)

## 從 ckptJson 將值賦予至當前檔案之變數中
if ckpt_bool:
    loader_rng.manual_seed(ckptJson['seed']['torch_generator_seed'])
    model.load_state_dict(ckptJson['state_dict'])
    optimizer.load_state_dict(ckptJson['optimizer'])
    scheduler.load_state_dict(ckptJson['scheduler'])
    random_split_rng.set_state(ckptJson["random_split_rng"])
    train_loader_rng.set_state(ckptJson["train_loader_rng"])
    val_loader_rng.set_state(ckptJson["val_loader_rng"])

### 開始訓練的 For 迴圈
for epoch in range(args.start_epoch, args.epochs):
```
### Hook Function 記錄點
```
### 開始訓練的 For 迴圈
for epoch in range(args.start_epoch, args.epochs):
    ## 儲存 Checkpoint
    thisCkpt.saveCheckpoint({
            'epoch': epoch,
            'arch': args.arch,
            'state_dict': model.state_dict(),
            'best_acc1': best_acc1,
            'optimizer': optimizer.state_dict(),
            'scheduler': scheduler.state_dict(),
            'random_split_rng': random_split_rng.get_state(),
            'train_loader_rng': train_loader_rng.get_state(),
            'val_loader_rng': val_loader_rng.get_state()
        })
```


## MyOutput
* 紀錄實驗數據
* 三個子模組:
    * setAndCheckPath: 建立預計輸出之路徑
    * saveTime: 紀錄各個事件的時間
    * paint_line_chart: 繪製直線圖

### setAndCheckPath: 統一建立預計輸出之路徑
```
from MyOutput import setAndCheckPath
# 建立與設定預計輸出的檔案路徑
output_path = setAndCheckPath.setAndCheckPath(
    [args.output_path, os.environ["dag_id"], os.environ["task_id"], os.environ["run_id"]])
```
### saveTime: 事件時間紀錄
```
from MyOutput import saveTime
saveTime.checkPathAndFile(output_path)
saveTime.recordTime("start")
```

### paint_line_chart：折線圖繪製
```
from MyOutput import paint_line_chart

# 初始化 輸出位置參數
paint_line_chart.checkPath(output_path)

# 繪製圖表
paint_line_chart.paint_line_chart(title="Accuracy",       
        label="train_Accuracy",data_list=train_acc_list, 
        label_2="vali_Accuracy", data_list_2=vali_acc_list
    )


```

## myKillProcess
自殺計時器，當計時器時間截止後，刪除該Pod

```
from myKillProcess import killprocess

# 初始化 自殺計時器
killStatus, timer = killprocess.setTimeToKill(
        sec=args.setTimeToKill, this_output_path=output_path)

# 開啟 自殺計時器
if args.setKill == "True" and killStatus:
    print("================================================")
    print("You set the process to be killed  after {} seconds. ".format(
        args.setTimeToKill))
    print("================================================")
    timer.start()

main()

# 關掉 自殺計時器
if args.setKill == "True" and killStatus:
    print("================================================")
    print("You canceled the timer of killing the process. ")
    print("================================================")
    timer.cancel()
```


from sklearn import metrics
import sklearn.metrics
import matplotlib.pyplot as plt
import argparse
import os
import random
import shutil
import time
import warnings
from enum import Enum

import torch
import torch.backends.cudnn as cudnn
import torch.distributed as dist
import torch.multiprocessing as mp
import torch.nn as nn
import torch.nn.parallel
import torch.optim
import torch.utils.data
import torch.utils.data.distributed
import torchvision.datasets as datasets
import torchvision.models as models
import torchvision.transforms as transforms
from torch.optim.lr_scheduler import StepLR
from torch.utils.data import Subset
from torch.utils.data import random_split
from torch.utils.data import Dataset
import os
from PIL import Image
import json

from MyOutput import paint_line_chart, saveTime, setAndCheckPath
from myKillProcess import killprocess
from myCkpt import checkpoint as thisCkpt

import numpy as np
import os

import matplotlib
matplotlib.use('Agg')

output_path = ""


model_names = sorted(name for name in models.__dict__
                     if name.islower() and not name.startswith("__")
                     and callable(models.__dict__[name]))

parser = argparse.ArgumentParser(description='PyTorch ImageNet Training')
parser.add_argument('data', metavar='DIR', nargs='?', default='imagenet',
                    help='path to dataset (default: imagenet)')
parser.add_argument('-a', '--arch', metavar='ARCH', default='resnet18',
                    choices=model_names,
                    help='model architecture: ' +
                    ' | '.join(model_names) +
                    ' (default: resnet18)')
parser.add_argument('-j', '--workers', default=4, type=int, metavar='N',
                    help='number of data loading workers (default: 4)')
parser.add_argument('--epochs', default=90, type=int, metavar='N',
                    help='number of total epochs to run')
parser.add_argument('--start-epoch', default=0, type=int, metavar='N',
                    help='manual epoch number (useful on restarts)')
parser.add_argument('-b', '--batch-size', default=256, type=int,
                    metavar='N',
                    help='mini-batch size (default: 256), this is the total '
                         'batch size of all GPUs on the current node when '
                         'using Data Parallel or Distributed Data Parallel')
parser.add_argument('--lr', '--learning-rate', default=0.1, type=float,
                    metavar='LR', help='initial learning rate', dest='lr')
parser.add_argument('--momentum', default=0.9, type=float, metavar='M',
                    help='momentum')
parser.add_argument('--wd', '--weight-decay', default=1e-4, type=float,
                    metavar='W', help='weight decay (default: 1e-4)',
                    dest='weight_decay')
parser.add_argument('-p', '--print-freq', default=10, type=int,
                    metavar='N', help='print frequency (default: 10)')
parser.add_argument('--resume', default='', type=str, metavar='PATH',
                    help='path to latest checkpoint (default: none)')
parser.add_argument('-e', '--evaluate', dest='evaluate', action='store_true',
                    help='evaluate model on validation set')
parser.add_argument('--pretrained', dest='pretrained', action='store_true',
                    help='use pre-trained model')
parser.add_argument('--world-size', default=-1, type=int,
                    help='number of nodes for distributed training')
parser.add_argument('--rank', default=-1, type=int,
                    help='node rank for distributed training')
parser.add_argument('--dist-url', default='tcp://224.66.41.62:23456', type=str,
                    help='url used to set up distributed training')
parser.add_argument('--dist-backend', default='nccl', type=str,
                    help='distributed backend')
parser.add_argument('--seed', default=None, type=int,
                    help='seed for initializing training. ')
parser.add_argument('--gpu', default=None, type=int,
                    help='GPU id to use.')
parser.add_argument('--multiprocessing-distributed', action='store_true',
                    help='Use multi-processing distributed training to launch '
                         'N processes per node, which has N GPUs. This is the '
                         'fastest way to use PyTorch for either single node or '
                         'multi node data parallel training')
parser.add_argument('--dummy', action='store_true',
                    help="use fake data to benchmark")
parser.add_argument('--dataset', default="/dataset",
                    type=str, help="set path of dataset")
parser.add_argument('--output_path', default="/dataset/output", type=str,
                    help="Set the storage location for the output parameters.")
parser.add_argument('--setKill', default="False", type=str,
                    help="Set whether to kill the process.")
parser.add_argument('--setTimeToKill', default=30, type=int,
                    help="Set the time to kill the process.")
parser.add_argument('--enableCKPT', default="True", type=str,
                    help="Set the status to indicate whether to enable CKPT Hook Function.")
parser.add_argument('--enableSeed', default="True", type=str,
                    help="Set the status to indicate whether to enable Seed Function.")
best_acc1 = 0

# SEED = 112525005

# os.environ['CUBLAS_WORKSPACE_CONFIG'] = ':4096:8'
# os.environ['PYTHONHASHSEED'] = str(SEED)

# def set_seed(seed):
#     torch.manual_seed(seed)
#     torch.cuda.manual_seed(seed)
#     torch.cuda.manual_seed_all(seed)  # if you are using multi-GPU.
#     np.random.seed(seed)
#     random.seed(seed)
#     torch.backends.cudnn.deterministic = True
#     torch.backends.cudnn.benchmark = False


def seed_worker(worker_id):
    worker_seed = torch.initial_seed() % 2**32
    np.random.seed(worker_seed)
    random.seed(worker_seed)


class ImageNetKaggle(Dataset):
    def __init__(self, root, split, transform=None):
        self.samples = []
        self.targets = []
        self.transform = transform
        self.syn_to_class = {}
        with open(os.path.join(root, "imagenet_class_index.json"), "rb") as f:
            json_file = json.load(f)
            for class_id, v in json_file.items():
                self.syn_to_class[v[0]] = int(class_id)
        with open(os.path.join(root, "ILSVRC2012_val_labels.json"), "rb") as f:
            self.val_to_syn = json.load(f)
        samples_dir = os.path.join(root, "ILSVRC/Data/CLS-LOC", split)
        for entry in os.listdir(samples_dir):
            if split == "train":
                syn_id = entry
                target = self.syn_to_class[syn_id]
                syn_folder = os.path.join(samples_dir, syn_id)
                for sample in os.listdir(syn_folder):
                    sample_path = os.path.join(syn_folder, sample)
                    self.samples.append(sample_path)
                    self.targets.append(target)
            elif split == "val":
                syn_id = self.val_to_syn[entry]
                target = self.syn_to_class[syn_id]
                sample_path = os.path.join(samples_dir, entry)
                self.samples.append(sample_path)
                self.targets.append(target)

    def __len__(self):
        return len(self.samples)

    def __getitem__(self, idx):
        x = Image.open(self.samples[idx]).convert("RGB")
        if self.transform:
            x = self.transform(x)
        return x, self.targets[idx]


def main():
    args = parser.parse_args()
    thisCkpt.loadCheckpoint(args.enableSeed)
    if args.seed is not None:
        random.seed(args.seed)
        torch.manual_seed(args.seed)
        cudnn.deterministic = True
        cudnn.benchmark = False
        warnings.warn('You have chosen to seed training. '
                      'This will turn on the CUDNN deterministic setting, '
                      'which can slow down your training considerably! '
                      'You may see unexpected behavior when restarting '
                      'from checkpoints.')

    if args.gpu is not None:
        warnings.warn('You have chosen a specific GPU. This will completely '
                      'disable data parallelism.')

    if args.dist_url == "env://" and args.world_size == -1:
        args.world_size = int(os.environ["WORLD_SIZE"])

    args.distributed = args.world_size > 1 or args.multiprocessing_distributed

    if torch.cuda.is_available():
        ngpus_per_node = torch.cuda.device_count()
        if ngpus_per_node == 1 and args.dist_backend == "nccl":
            warnings.warn(
                "nccl backend >=2.5 requires GPU count>1, see https://github.com/NVIDIA/nccl/issues/103 perhaps use 'gloo'")
    else:
        ngpus_per_node = 1

    if args.multiprocessing_distributed:
        # Since we have ngpus_per_node processes per node, the total world_size
        # needs to be adjusted accordingly
        args.world_size = ngpus_per_node * args.world_size
        # Use torch.multiprocessing.spawn to launch distributed processes: the
        # main_worker process function
        mp.spawn(main_worker, nprocs=ngpus_per_node,
                 args=(ngpus_per_node, args))
    else:
        # Simply call main_worker function
        main_worker(args.gpu, ngpus_per_node, args)


def main_worker(gpu, ngpus_per_node, args):

    global best_acc1
    args.gpu = gpu

    if args.gpu is not None:
        print("Use GPU: {} for training".format(args.gpu))

    if args.distributed:
        if args.dist_url == "env://" and args.rank == -1:
            args.rank = int(os.environ["RANK"])
        if args.multiprocessing_distributed:
            # For multiprocessing distributed training, rank needs to be the
            # global rank among all the processes
            args.rank = args.rank * ngpus_per_node + gpu
        dist.init_process_group(backend=args.dist_backend, init_method=args.dist_url,
                                world_size=args.world_size, rank=args.rank)
    # create model
    if args.pretrained:
        print("=> using pre-trained model '{}'".format(args.arch))
        model = models.__dict__[args.arch](pretrained=True)
    else:
        print("=> creating model '{}'".format(args.arch))
        model = models.__dict__[args.arch]()

    if not torch.cuda.is_available() and not torch.backends.mps.is_available():
        print('using CPU, this will be slow')
    elif args.distributed:
        # For multiprocessing distributed, DistributedDataParallel constructor
        # should always set the single device scope, otherwise,
        # DistributedDataParallel will use all available devices.
        if torch.cuda.is_available():
            if args.gpu is not None:
                torch.cuda.set_device(args.gpu)
                model.cuda(args.gpu)
                # When using a single GPU per process and per
                # DistributedDataParallel, we need to divide the batch size
                # ourselves based on the total number of GPUs of the current node.
                args.batch_size = int(args.batch_size / ngpus_per_node)
                args.workers = int(
                    (args.workers + ngpus_per_node - 1) / ngpus_per_node)
                model = torch.nn.parallel.DistributedDataParallel(
                    model, device_ids=[args.gpu])
            else:
                model.cuda()
                # DistributedDataParallel will divide and allocate batch_size to all
                # available GPUs if device_ids are not set
                model = torch.nn.parallel.DistributedDataParallel(model)
    elif args.gpu is not None and torch.cuda.is_available():
        torch.cuda.set_device(args.gpu)
        model = model.cuda(args.gpu)
    elif torch.backends.mps.is_available():
        device = torch.device("mps")
        model = model.to(device)
    else:
        # DataParallel will divide and allocate batch_size to all available GPUs
        if args.arch.startswith('alexnet') or args.arch.startswith('vgg'):
            model.features = torch.nn.DataParallel(model.features)
            model.cuda()
        else:
            model = torch.nn.DataParallel(model).cuda()

    if torch.cuda.is_available():
        if args.gpu:
            print('using GPU ......cuda:{}'.format(args.gpu))
            device = torch.device('cuda:{}'.format(args.gpu))
        else:
            print('using GPU ......')
            device = torch.device("cuda")
    elif torch.backends.mps.is_available():
        device = torch.device("mps")
    else:
        print('using CPU ......')
        device = torch.device("cpu")
    # define loss function (criterion), optimizer, and learning rate scheduler
    criterion = nn.CrossEntropyLoss().to(device)

    optimizer = torch.optim.SGD(model.parameters(), args.lr,
                                momentum=args.momentum,
                                weight_decay=args.weight_decay)

    """Sets the learning rate to the initial LR decayed by 10 every 30 epochs"""
    scheduler = StepLR(optimizer, step_size=30, gamma=0.1)

    # optionally resume from a checkpoint
    if args.resume:
        if os.path.isfile(args.resume):
            print("=> loading checkpoint '{}'".format(args.resume))
            if args.gpu is None:
                checkpoint = torch.load(args.resume)
            elif torch.cuda.is_available():
                # Map model to be loaded to specified single gpu.
                loc = 'cuda:{}'.format(args.gpu)
                checkpoint = torch.load(args.resume, map_location=loc)
            args.start_epoch = checkpoint['epoch']
            best_acc1 = checkpoint['best_acc1']
            if args.gpu is not None:
                # best_acc1 may be from a checkpoint from a different GPU
                best_acc1 = best_acc1.to(args.gpu)
            model.load_state_dict(checkpoint['state_dict'])
            optimizer.load_state_dict(checkpoint['optimizer'])
            scheduler.load_state_dict(checkpoint['scheduler'])
            print("=> loaded checkpoint '{}' (epoch {})"
                  .format(args.resume, checkpoint['epoch']))

        else:
            print("=> no checkpoint found at '{}'".format(args.resume))

    # Data loading code
    if args.dummy:
        print("=> Dummy data is used!")
        train_dataset = datasets.FakeData(
            1281167, (3, 224, 224), 1000, transforms.ToTensor())
        val_dataset = datasets.FakeData(
            50000, (3, 224, 224), 1000, transforms.ToTensor())
    else:
        traindir = os.path.join(args.data, 'train')
        # valdir = os.path.join(args.data, 'val')
        normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                         std=[0.229, 0.224, 0.225])

        train_dataset = ImageNetKaggle(args.dataset+"/imagenet-object-localization-challenge/", "train",  transforms.Compose([
            transforms.Resize(256),
            transforms.CenterCrop(224),
            transforms.ToTensor(),
            normalize,
        ]))

        val_dataset = ImageNetKaggle(args.dataset+"/imagenet-object-localization-challenge/", "val",  transforms.Compose([
            transforms.Resize(256),
            transforms.CenterCrop(224),
            transforms.ToTensor(),
            normalize,
        ]))

        random_split_rng = torch.Generator()
        val_dataset, test_dataset = random_split(
            dataset=val_dataset,
            lengths=[0.5 , 0.5],
            generator=random_split_rng
        )

    if args.distributed:
        train_sampler = torch.utils.data.distributed.DistributedSampler(
            train_dataset)
        val_sampler = torch.utils.data.distributed.DistributedSampler(
            val_dataset, shuffle=False, drop_last=True)
    else:
        train_sampler = None
        val_sampler = None

    train_loader_rng = torch.Generator()
    val_loader_rng = torch.Generator()

    train_loader = torch.utils.data.DataLoader(
        train_dataset, batch_size=args.batch_size, shuffle=(
            train_sampler is None),
        num_workers=args.workers, pin_memory=True, sampler=train_sampler,
        generator=train_loader_rng
    )

    val_loader = torch.utils.data.DataLoader(
        val_dataset, batch_size=args.batch_size, shuffle=False,
        num_workers=args.workers, pin_memory=True, sampler=val_sampler,
        generator=val_loader_rng
    )

    test_loader = torch.utils.data.DataLoader(
        test_dataset, batch_size=args.batch_size, shuffle=False,
        num_workers=args.workers, pin_memory=True, sampler=val_sampler,
        generator=val_loader_rng
    )

    if args.enableCKPT == "True":
        print("==============================")
        print("You enable the CKPT function.")
        print("==============================")

        # 系統備份 CKPT
        saveTime.recordTime("init_{}".format("loadCheckpoint"))
        ckpt_bool, ckptJson = thisCkpt.loadCheckpoint(args.enableSeed)
        saveTime.recordTime("init_{}".format("loadCheckpoint_Finish"))
        train_loader_rng.manual_seed(ckptJson['seed']['torch_generator_seed'])
        val_loader_rng.manual_seed(ckptJson['seed']['torch_generator_seed'])
        random_split_rng.manual_seed(ckptJson['seed']['torch_generator_seed'])
        # print("ckptJson:",ckptJson)
        if ckpt_bool:
            args.start_epoch = ckptJson['epoch'] + 1
            best_acc1 = ckptJson['best_acc1']
            if args.gpu is not None:
                # best_acc1 may be from a checkpoint from a different GPU
                best_acc1 = best_acc1.to(args.gpu)
            model.load_state_dict(ckptJson['state_dict'])
            optimizer.load_state_dict(ckptJson['optimizer'])
            scheduler.load_state_dict(ckptJson['scheduler'])
            # torch.set_rng_state(ckptJson['torch_rng'])
            # torch.cuda.set_rng_state(ckptJson['cuda_rng'])
            # np.random.set_state(ckptJson['numpy_rng'])
            # random.setstate(ckptJson['python_rng'])
            random_split_rng.set_state(ckptJson["random_split_rng"])
            train_loader_rng.set_state(ckptJson["train_loader_rng"])
            val_loader_rng.set_state(ckptJson["val_loader_rng"])

    if args.evaluate:
        validate(test_loader, model, criterion, args)
        return

    # 開始訓練
    train_loss_list = []
    train_acc_list = []
    vali_loss_list = []
    vali_acc_list = []

    # 判斷或建立路徑，以利Checkpoint file 的存檔
    global output_path

    print(" ============ set output path ================= ")
    print(" => output path of container: ", output_path)
    if not os.path.isdir(output_path):
        raise FileNotFoundError(
            output_path+"\n You need call setAndCheckPath function!")

    if args.start_epoch != 0:
        try:
            train_loss_list = np.genfromtxt(
                "{}/train_Loss.csv".format(output_path), dtype=str, delimiter=",").tolist()
            train_acc_list = np.genfromtxt(
                "{}/train_acc_list.csv".format(output_path), dtype=str, delimiter=",").tolist()
            vali_loss_list = np.genfromtxt(
                "{}/vali_loss_list.csv".format(output_path), dtype=str, delimiter=",").tolist()
            vali_acc_list = np.genfromtxt(
                "{}/vali_acc_list.csv".format(output_path), dtype=str, delimiter=",").tolist()
            print(train_loss_list)
        except:
            print(" => Csv file of Acc & Loss doesn't exit!!! ")
            train_loss_list = []
            train_acc_list = []
            vali_loss_list = []
            vali_acc_list = []

    print(" ============ training task start ================= ")
    for epoch in range(args.start_epoch, args.epochs):
        saveTime.recordTime("epoch_{}".format(epoch))  # 紀錄 Epoch 開始執行的時間

        if args.distributed:
            train_sampler.set_epoch(epoch)

        # train for one epoch
        train_top1_acc, train_losses = train(
            train_loader, model, criterion, optimizer, epoch, device, args)

        # evaluate on validation set
        vali_acc1, vali_loss, predictions, valid_targets = validate(
            val_loader, model, criterion, args)

        scheduler.step()

        # remember best acc@1 and save checkpoint
        is_best = vali_acc1 > best_acc1
        best_acc1 = max(vali_acc1, best_acc1)
        if args.enableCKPT == "True":
            # 紀錄此Training 狀態
            saveTime.recordTime("epoch_{}_{}".format(epoch, "saveCKPT"))
            thisCkpt.saveCheckpoint({
                'epoch': epoch,
                'arch': args.arch,
                'state_dict': model.state_dict(),
                'best_acc1': best_acc1,
                'optimizer': optimizer.state_dict(),
                'scheduler': scheduler.state_dict(),
                # 'torch_rng': torch.get_rng_state(),
                # 'cuda_rng': torch.cuda.get_rng_state(),
                # 'numpy_rng': np.random.get_state(),
                # 'python_rng': random.getstate(),
                'random_split_rng': random_split_rng.get_state(),
                'train_loader_rng': train_loader_rng.get_state(),
                'val_loader_rng': val_loader_rng.get_state()
            })
            saveTime.recordTime("epoch_{}_{}".format(epoch, "saveCKPT_Finish"))

        if not args.multiprocessing_distributed or (args.multiprocessing_distributed
                                                    and args.rank % ngpus_per_node == 0):
            save_checkpoint({
                'epoch': epoch,
                'arch': args.arch,
                'state_dict': model.state_dict(),
                'best_acc1': best_acc1,
                'optimizer': optimizer.state_dict(),
                'scheduler': scheduler.state_dict()
            }, is_best)

        # List 存檔

        # 添加至 Output 中
        train_loss_list.append(train_losses)
        train_acc_list.append(train_top1_acc.cpu())
        vali_loss_list.append(vali_loss)
        vali_acc_list.append(vali_acc1.cpu())

        # List 數據存檔
        np.savetxt("{}/train_Loss.csv".format(output_path),
                   train_loss_list, delimiter=",", fmt='%s')
        np.savetxt("{}/train_Accuracy.csv".format(output_path),
                   train_acc_list, delimiter=",", fmt='%s')
        np.savetxt("{}/vali_Loss.csv".format(output_path),
                   vali_loss_list, delimiter=",", fmt='%s')
        np.savetxt("{}/vali_Accuracy.csv".format(output_path),
                   vali_acc_list, delimiter=",", fmt='%s')

        # 畫圖
        paint_line_chart.paint_line_chart(title="Accuracy", label="train_Accuracy",
                                          data_list=train_acc_list, label_2="vali_Accuracy", data_list_2=vali_acc_list)
        paint_line_chart.paint_line_chart(
            title="Loss", label="train_Loss", data_list=train_loss_list, label_2="vali_Loss", data_list_2=vali_loss_list)

        # # AUC、ROC metrics
        # roc_auc = metrics.roc_auc_score(valid_targets, predictions)
        # fpr, tpr, thresholds = sklearn.metrics.roc_curve(valid_targets, predictions)

        # # Plot and save ROC curve
        # fig = plt.figure()
        # plt.title('ROC - lfw dev-test')
        # plt.plot(fpr, tpr, lw=2, label='ROC (auc = %0.4f)' % roc_auc)
        # plt.xlim([0.0, 1.0])
        # plt.ylim([0.0, 1.05])
        # plt.grid()
        # plt.xlabel('False Positive Rate')
        # plt.ylabel('True Positive Rate')
        # plt.legend(loc='lower right')
        # plt.tight_layout()

        # print(f"Epoch={epoch}, Valid ROC AUC={roc_auc}")
    # path = "/dataset/output"
    # plt.savefig(path+"/ROC_curve.png", bbox_inches='tight')
    # print('ROC curve saved at: ' + path)


def train(train_loader, model, criterion, optimizer, epoch, device, args):
    batch_time = AverageMeter('Time', ':6.3f')
    data_time = AverageMeter('Data', ':6.3f')
    losses = AverageMeter('Loss', ':.4e')
    top1 = AverageMeter('Acc@1', ':6.2f')
    top5 = AverageMeter('Acc@5', ':6.2f')
    progress = ProgressMeter(
        len(train_loader),
        [batch_time, data_time, losses, top1, top5],
        prefix="Epoch: [{}]".format(epoch))

    # switch to train mode
    model.train()

    end = time.time()
    for i, (images, target) in enumerate(train_loader):
        # measure data loading time
        data_time.update(time.time() - end)

        # move data to the same device as model
        images = images.to(device, non_blocking=True)
        target = target.to(device, non_blocking=True)

        # compute output
        output = model(images)
        loss = criterion(output, target)

        # measure accuracy and record loss
        acc1, acc5 = accuracy(output, target, topk=(1, 5))
        losses.update(loss.item(), images.size(0))
        top1.update(acc1[0], images.size(0))
        top5.update(acc5[0], images.size(0))

        # compute gradient and do SGD step
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        # measure elapsed time
        batch_time.update(time.time() - end)
        end = time.time()

        if i % args.print_freq == 0:
            progress.display(i + 1)
    return top1.avg, losses.avg


def validate(val_loader, model, criterion, args):

    def run_validate(loader, base_progress=0):
        with torch.no_grad():
            end = time.time()
            for i, (images, target) in enumerate(loader):
                i = base_progress + i
                if args.gpu is not None and torch.cuda.is_available():
                    images = images.cuda(args.gpu, non_blocking=True)
                if torch.backends.mps.is_available():
                    images = images.to('mps')
                    target = target.to('mps')
                if torch.cuda.is_available():
                    target = target.cuda(args.gpu, non_blocking=True)

                # compute output
                output = model(images)
                loss = criterion(output, target)

                # measure accuracy and record loss
                acc1, acc5 = accuracy(output, target, topk=(1, 5))
                losses.update(loss.item(), images.size(0))
                top1.update(acc1[0], images.size(0))
                top5.update(acc5[0], images.size(0))

                # 轉換 AUC ROC 的元素
                target_1 = target.detach().cpu().numpy().tolist()
                output_1 = output.detach().cpu().numpy().tolist()

                # 添加到陣列之中
                final_targets.extend(target_1)
                final_outputs.extend(output_1)

                # measure elapsed time
                batch_time.update(time.time() - end)
                end = time.time()

                if i % args.print_freq == 0:
                    progress.display(i + 1)

    # 製作 AUC ROC 的陣列
    final_outputs = []
    final_targets = []

    # 相關參數
    batch_time = AverageMeter('Time', ':6.3f', Summary.NONE)
    losses = AverageMeter('Loss', ':.4e', Summary.NONE)
    top1 = AverageMeter('Acc@1', ':6.2f', Summary.AVERAGE)
    top5 = AverageMeter('Acc@5', ':6.2f', Summary.AVERAGE)
    progress = ProgressMeter(
        len(val_loader) + (args.distributed and (len(val_loader.sampler)
                                                 * args.world_size < len(val_loader.dataset))),
        [batch_time, losses, top1, top5],
        prefix='Test: ')

    # switch to evaluate mode
    model.eval()

    run_validate(val_loader)

    if args.distributed:
        top1.all_reduce()
        top5.all_reduce()

    if args.distributed and (len(val_loader.sampler) * args.world_size < len(val_loader.dataset)):
        aux_val_dataset = Subset(val_loader.dataset,
                                 range(len(val_loader.sampler) * args.world_size, len(val_loader.dataset)))
        aux_val_loader = torch.utils.data.DataLoader(
            aux_val_dataset, batch_size=args.batch_size, shuffle=False,
            num_workers=args.workers, pin_memory=True
        )
        run_validate(aux_val_loader, len(val_loader))

    progress.display_summary()

    return top1.avg, losses.avg, final_outputs, final_targets


def save_checkpoint(state, is_best, filename='checkpoint.pth.tar'):
    torch.save(state, filename)
    if is_best:
        shutil.copyfile(filename, 'model_best.pth.tar')


class Summary(Enum):
    NONE = 0
    AVERAGE = 1
    SUM = 2
    COUNT = 3


class AverageMeter(object):
    """Computes and stores the average and current value"""

    def __init__(self, name, fmt=':f', summary_type=Summary.AVERAGE):
        self.name = name
        self.fmt = fmt
        self.summary_type = summary_type
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count

    def all_reduce(self):
        if torch.cuda.is_available():
            device = torch.device("cuda")
        elif torch.backends.mps.is_available():
            device = torch.device("mps")
        else:
            device = torch.device("cpu")
        total = torch.tensor([self.sum, self.count],
                             dtype=torch.float32, device=device)
        dist.all_reduce(total, dist.ReduceOp.SUM, async_op=False)
        self.sum, self.count = total.tolist()
        self.avg = self.sum / self.count

    def __str__(self):
        fmtstr = '{name} {val' + self.fmt + '} ({avg' + self.fmt + '})'
        return fmtstr.format(**self.__dict__)

    def summary(self):
        fmtstr = ''
        if self.summary_type is Summary.NONE:
            fmtstr = ''
        elif self.summary_type is Summary.AVERAGE:
            fmtstr = '{name} {avg:.3f}'
        elif self.summary_type is Summary.SUM:
            fmtstr = '{name} {sum:.3f}'
        elif self.summary_type is Summary.COUNT:
            fmtstr = '{name} {count:.3f}'
        else:
            raise ValueError('invalid summary type %r' % self.summary_type)

        return fmtstr.format(**self.__dict__)


class ProgressMeter(object):
    def __init__(self, num_batches, meters, prefix=""):
        self.batch_fmtstr = self._get_batch_fmtstr(num_batches)
        self.meters = meters
        self.prefix = prefix

    def display(self, batch):
        entries = [self.prefix + self.batch_fmtstr.format(batch)]
        entries += [str(meter) for meter in self.meters]
        print('\t'.join(entries))

    def display_summary(self):
        entries = [" *"]
        entries += [meter.summary() for meter in self.meters]
        print(' '.join(entries))

    def _get_batch_fmtstr(self, num_batches):
        num_digits = len(str(num_batches // 1))
        fmt = '{:' + str(num_digits) + 'd}'
        return '[' + fmt + '/' + fmt.format(num_batches) + ']'


def accuracy(output, target, topk=(1,)):
    """Computes the accuracy over the k top predictions for the specified values of k"""
    with torch.no_grad():
        maxk = max(topk)
        batch_size = target.size(0)

        _, pred = output.topk(maxk, 1, True, True)
        pred = pred.t()
        correct = pred.eq(target.view(1, -1).expand_as(pred))

        res = []
        for k in topk:
            correct_k = correct[:k].reshape(-1).float().sum(0, keepdim=True)
            res.append(correct_k.mul_(100.0 / batch_size))
        return res


if __name__ == '__main__':
    from myCkpt import airflowTaskID

    # 取得參數值
    args = parser.parse_args()

    # 取得與設定此任務的 TaskID
    dag_id, run_id, task_id = airflowTaskID.getAirflowTaskID()

    # 建立與設定預計輸出的檔案路徑
    output_path = setAndCheckPath.setAndCheckPath(
        [args.output_path, os.environ["dag_id"], os.environ["task_id"], os.environ["run_id"]])

    # 設定檔案輸出路徑
    paint_line_chart.checkPath(output_path)
    saveTime.checkPathAndFile(output_path)  # 設定檔案輸出路徑, 檔案命名

    # 紀錄程式啟動時間
    saveTime.recordTime("start")  # 紀錄啟動時間

    # 取得自殺計時器
    killStatus, timer = killprocess.setTimeToKill(
        sec=args.setTimeToKill, this_output_path=output_path)  # 在 XX 秒後 自動 kill 自己
    print(killStatus)

    # 開啟自殺計時器
    if args.setKill == "True" and killStatus:
        print("================================================")
        print("You set the process to be killed  after {} seconds. ".format(
            args.setTimeToKill))
        print("================================================")
        timer.start()

    main()

    # 紀錄結束時間
    saveTime.recordTime("finish")

    # 關掉自殺計時器
    if args.setKill == "True" and killStatus:
        print("================================================")
        print("You canceled the timer of killing the process. ")
        print("================================================")
        timer.cancel()

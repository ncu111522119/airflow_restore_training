import torch
import os
import socket
import myCkpt.airflowTaskID as airflowTaskID
import myCkpt.minioFileProcess as minioFileProcess
import configparser
import random
import numpy
config = configparser.ConfigParser()
config.read("./myCkpt/config.ini")
print("===== Checkpoint =====")

# dag_id+task_id
run_id = ""  # "resnet18_local"
dag_id = ""
task_id = ""
checkpoint_dir = "./checkpoint/"
bucketName = "my-bucket"
PodIP = ""



torch_generator_seed = None
np_seed = None
torch_gpu_seed = None
torch_seed = None
random_seed = None
get_num_threads = None

"""
取得 Seed ，並存入本檔案的全域變數中。
"""
def get_Seed():
    global torch_generator_seed
    global np_seed
    global torch_gpu_seed
    global torch_seed
    global random_seed
    global get_num_threads

    if np_seed == None:
        # 將 Seed 設置為全域變數，避免儲存時機不同，使 Seed 不一致
        print("getSeed()：還沒拿到Seed，取得 Seed")
        np_seed =  numpy.random.get_state()
        torch_gpu_seed = torch.cuda.initial_seed()
        torch_seed = torch.random.initial_seed() #torch.seed()
        random_seed = random.getstate()
        torch_generator_seed = torch.Generator().seed()
        get_num_threads = torch.get_num_threads() # 新增 Thread 的數量
    else:
        print("getSeed()：已經有 Seed 了， Seed 在全域變數中")

    # print("torch_generator_seed",torch_generator_seed)

    return {"np_seed": np_seed, "torch_gpu_seed": torch_gpu_seed, "torch_seed": torch_seed, "random_seed": random_seed, "torch_generator_seed": torch_generator_seed, "get_num_threads": get_num_threads}


def get_RNGState():
   return {
        'torch_rng': torch.get_rng_state(),
        'cuda_rng': torch.cuda.get_rng_state(),
        'numpy_rng': numpy.random.get_state(),
        'python_rng': random.getstate(),
    }
    

"""
設定 Seed ，並存入本檔案的全域變數中。
"""
def set_Seed(seed:dict):
    
    ## Seed 設置為全域變數
    ### 每次取得的 Seed 會不同，為避免儲存時機不同，因此將 Seed 設置為全域變數，避免取得的 Seed 不一致
    global torch_generator_seed
    global np_seed
    global torch_gpu_seed
    global torch_seed
    global random_seed
    global get_num_threads

    if np_seed == None:
        print("Seed 設置為全域變數")
        np_seed = seed["np_seed"]
        torch_gpu_seed = seed["torch_gpu_seed"]
        torch_seed = seed["torch_seed"]
        random_seed = seed["random_seed"]
        torch_generator_seed = seed["torch_generator_seed"]
        get_num_threads = seed["get_num_threads"]
   
    ## 設定 Random Seed
    print("1. 設定 Seed： torch、torch.cuda、numpy、random")
    torch.manual_seed(torch_seed)
    torch.cuda.manual_seed(torch_gpu_seed)
    torch.cuda.manual_seed_all(torch_gpu_seed)
    # torch.set_num_threads(get_num_threads)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False
    torch.use_deterministic_algorithms(True)
    numpy.random.set_state(np_seed)
    random.setstate(random_seed)
    os.environ['PYTHONHASHSEED'] = str(random_seed)
    os.environ["CUBLAS_WORKSPACE_CONFIG"] = ":4096:8"



def set_RNGState(rngState:dict):
    torch.set_rng_state(rngState['torch_rng'])
    torch.cuda.set_rng_state(rngState['cuda_rng'])
    numpy.random.set_state(rngState['numpy_rng'])
    random.setstate(rngState['python_rng'])


"""
DataLoader 在設定Seed 時，所需要的 Func。
需在使用 DataLoader時，帶入 worker_init_fn:function 的參數。
"""
def seed_worker(worker_id):
    worker_seed = torch.initial_seed() % 2**32
    numpy.random.seed(worker_seed)
    random.seed(worker_seed)

"""
設定 DataLoader 的 Seed。
需在使用 DataLoader時，帶入  generator:function 的參數。 
Input:
- this_torch_generator_seed: python dataloader seed
"""
def set_torch_generator_seed(this_torch_generator_seed):
    return torch.Generator().manual_seed(this_torch_generator_seed)


"""
1. 將參數存為 CKPT
1.1 Model
1.2 Seed
2. 以 Airflow ID 命名
3. 上傳至 Minio Repository，並以Prefix存檔
"""
def saveCheckpoint(CkptJSON):
    global dag_id, run_id, task_id
    # 判斷或建立路徑，以利Checkpoint file 的存檔
    if not os.path.isdir(checkpoint_dir):
        os.mkdir(checkpoint_dir)

    if run_id == "":
        # 取得 Airflow 系統任務對應的 TaskID
        dag_id, run_id, task_id = airflowTaskID.getAirflowTaskID_withKubeAPI(
            PodIP)

    # 相關需要的參數
    fileName = "{}/{}/{}.pt".format(dag_id, task_id, run_id) # "minioPrefix/fileName"
    ckptPath = checkpoint_dir+fileName
    CkptJSON["seed"] = get_Seed()
    CkptJSON["RNGState"] = get_RNGState()
    # set_Seed(CkptJSON["seed"])
    # 設置 Checkpoint
    torch.save(CkptJSON, ckptPath)

    # 將Checkpoint 檔案上傳至 Minio
    minioFileProcess.uploadFile(bucketName, ckptPath, fileName)


"""
1. 以 Airflow ID 並從 Minio Prefix至 Minio Repository 找到CKPT file
2. 根據 enableSeed 需求判定是否存取 Seed
3. 將解析的參數回傳
"""
def loadCheckpoint(enableSeed="True"):
    
    # 取得 Airflow 系統任務對應的 TaskID
    global dag_id, run_id, task_id
    if run_id == "":
        dag_id, run_id, task_id = airflowTaskID.getAirflowTaskID_withKubeAPI(PodIP)

    # 相關需要的參數
    fileName = "{}/{}/{}.pt".format(dag_id, task_id, run_id)
    ckptPath = checkpoint_dir+fileName

    # 將Checkpoint 檔案下載至 Local
    isfile = minioFileProcess.downloadFile(bucketName, ckptPath, fileName)
    
    

    if os.path.isfile(ckptPath) | isfile:
        print("檔案存於Minio: {}。".format(fileName))
        CkptJSON = torch.load(ckptPath)

        # 將現在的環境中設定 原Checkpoint 的 Random Seed
        if 'seed' in CkptJSON.keys() and enableSeed == "True":
            print("loadCheckpoint()：seed is exit")
            set_Seed(CkptJSON["seed"])
            if 'seed' in CkptJSON.keys():
                set_RNGState(CkptJSON["RNGState"])

        elif enableSeed == "False" :
            print("loadCheckpoint()：不使用 Seed")
            
        else:
            print("loadCheckpoint()：seed isn't exit")
            CkptJSON = {"seed":get_Seed()}
            set_Seed(CkptJSON["seed"])

        print("==== loaded checkpoint '{}' (epoch {}) =====".format(
            run_id, CkptJSON['epoch']))
        
        return True, CkptJSON
    
    else:
        print("loadCheckpoint()：檔案不存在。："+ckptPath)
        epoch = 1
        # 取得目前的環境的 Random seed，並設定
        CkptJSON = {"seed":get_Seed()}
        set_Seed(CkptJSON["seed"])
        return False, CkptJSON


# if __name__ == '__main__':
#     PodIP = '10.244.2.231'
#     airflowTaskID.getAirflowTaskID_withKubeAPI(PodIP)

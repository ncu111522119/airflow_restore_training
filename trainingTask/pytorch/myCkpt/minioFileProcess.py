from minio import Minio
import configparser
import os
import sys

# from pathlib import Path
# print(Path.cwd())

# print(sys.path)
print("===== Checkpoint =====")

config = configparser.ConfigParser()
config.read(r"./myCkpt/config.ini")
print(config.get("Minio","endpoint"))


# 建立 Client 通道
client = Minio(
    endpoint=config['Minio']['endpoint'],
    access_key=config['Minio']['access_key'],
    secret_key=config['Minio']['secret_key'],
    secure=config.getboolean('Minio','secure')
)


"""
上傳檔案至 Minio
Input:
- bucketName: 預計上傳的 bucket
- filePath: 預計上傳的本地檔案路徑
- fileName: 檔案名稱，含 Minio Prefix(資料夾)， my/prefix/fileName
Output:
"""
def uploadFile(bucketName,filePath,fileName):
    
    print(f'bucketName: {bucketName}\t filePath: {filePath}\t fileName: {fileName}')
    
    # 判斷 Bucket 是否存在
    ## 不存在就建立
    if client.bucket_exists(bucketName):
        print("my-bucket exists")
    else:
        print("my-bucket does not exist")
        client.make_bucket(bucketName)

    # 檢查檔案是否存在
    print("檔案:"+filePath)
    if os.path.isfile(filePath):
        print(": 檔案存在。")
    else:
        print("minioFileProc: 檔案不存在。："+filePath)

    # 上傳本地端的檔案
    ## fput_object、put_object: 上傳串流檔案
    result = client.fput_object(
        bucket_name=bucketName,
        object_name=fileName,
        file_path=filePath,
    )

    print(
        "created {0} object; etag: {1}, version-id: {2}".format(
            result.object_name, result.etag, result.version_id,
        ),
    )

    

"""
從 Minio 下載檔案
Input:
- bucketName: 預計上傳的 bucket
- filePath: 預計下載到本地的檔案路徑
- fileName: 檔案名稱，含 Minio Prefix， my/prefix/fileName
Output:

"""
def downloadFile(bucketName,filePath,fileName):
    try:
        client.fget_object(
                bucket_name=bucketName, 
                object_name=fileName, 
                file_path=filePath
            )
        return True
    except Exception as e:   
        print("\n======= Exception =======")
        print(e)
        print("\n")
        return False 
    



if __name__ == '__main__':
    
    uploadFile("my-bucket","../checkpoint/","MnistCkpt.pt")
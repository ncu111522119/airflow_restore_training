import configparser


# print(sys.path)
print("===== airflowTaskID =====")

"""
隱私資料
"""
config = configparser.ConfigParser()
config.read(r"./pytorch/myCkpt/config.ini")

"""
從環境變數取得目前的 Airflow Task ID
"""
def getAirflowTaskID():
    import os
     
    dag_id = ""
    run_id = ""
    task_id = ""
    
    if os.environ.get('run_id') == None:
        dag_id,run_id,task_id = getAirflowTaskID_withKubeAPI() 
        # print(dag_id,run_id,task_id )
    
   
    os.environ['dag_id'] = os.environ.get('dag_id', dag_id)
    os.environ['run_id'] = os.environ.get('run_id', run_id)
    os.environ['task_id'] = os.environ.get('task_id', task_id)
    
    print(dag_id,run_id,task_id)
    
    return dag_id,run_id,task_id


"""
從 Task ID Manager 取得目前 Pod Label ，從 Pod Label 解析出 Airflow Task ID 
"""
def getAirflowTaskID_withKubeAPI(PodIP=""):
    import requests
    import json
    import socket
    import os

    PodIP = socket.gethostbyname(socket.gethostname()) if PodIP == "" else PodIP
    # print("This Host's IP is", PodIP)
    url = config['APIServer']['url'] if config.has_section('APIServer') else os.environ["getAirflowTaskID_url"]

    payload = json.dumps({
        "PodIP": PodIP
    })
    headers = {
    'Content-Type': 'application/json'
    }

    response = requests.request("POST", url, headers=headers, data=payload)
    # print(response.text)
    response = eval(response.text)["result"]
    
    if response != "":
        dag_id = response["dag_id"]
        run_id = os.environ.get('run_id', response["run_id"])
        task_id = response["task_id"]
    else:
        print("==========================================")
        print("The task isn't in Airflow cluster!")
        print("run_id = 'not_airflow_task'")
        print("==========================================")
        dag_id = "dagid"
        run_id = os.environ.get('run_id', "runid_not_airflow_task")
        task_id = "taskid"
    
    return dag_id,run_id,task_id

"""
從 Airflow ORM 取得 DB 的 Task ID
"""
# def getAirflowTaskID_withORM():
#     from airflow.models.taskinstance import TaskInstance
#     from sqlalchemy import create_engine
#     from sqlalchemy.orm import Session
#     # PostgreSQL 相關訊息
#     account = config['PostgreSQL']['account']
#     password = config['PostgreSQL']['password']
#     DBIp = config['PostgreSQL']['DBIp']
#     DBPort = config['PostgreSQL']['DBPort']
#     # 建立 PostgreSQL 的通道
#     engine = create_engine(f"postgresql://{account}:{password}@{DBIp}:{DBPort}")
#     session = Session(engine)

#     # 取得 PodIP
#     PodIP = socket.gethostbyname(socket.gethostname())
#     print("This Host's IP is", PodIP)

#     # 查詢 Airflow System 資料
#     task_instances = session.query(TaskInstance).filter(
#         TaskInstance.hostname == PodIP).first()
#     if task_instances != None:
#         print("Relevant Information about Tasks in the Airflow System is ", (task_instances.dag_id, task_instances.run_id, task_instances.task_id))
#         return task_instances.dag_id, task_instances.run_id, task_instances.task_id
#     else:
#         print("The task isn't in Airflow cluster!")
#         return None, "not_airflow_task", None
from MyOutput import saveTime
import os
import sys
import signal
import numpy as np

killStatus_file="./killStatus.txt"

def killTheProcess(PodIP:str=""):
    import requests
    import json
    import socket
    import os

    global killStatus_file

    # this_killStatus_file = '{}/{}'.format(output_path,killStatus_file)
    # 將是否自殺過的狀態保存
    killStatus = np.loadtxt(killStatus_file,delimiter=' ', dtype=str)

    if killStatus == "True":
        saveTime.recordTime("killed") #被殺掉的時間
        np.savetxt(killStatus_file, ["False"], fmt="%s", delimiter=' ')
        print("自殺中 .....")
        PodIP = socket.gethostbyname(socket.gethostname()) if PodIP == "" else PodIP
        # print("This Host's IP is", PodIP)
        url = os.environ["killPod_url"]

        payload = json.dumps({
            "PodIP": PodIP
        })
        headers = {
        'Content-Type': 'application/json'
        }

        response = requests.request("POST", url, headers=headers, data=payload)
        # print(response.text)
        try:
            response = eval(response.text)["result"]
            dag_id = response["dag_id"]
            run_id = response["run_id"]
            task_id = response["task_id"]
            print("{}/{}/{} is killed .....".format(dag_id,run_id,task_id))
        except:
            pid = os.getpid()
            os.kill(pid, signal.SIGTERM)
            print("Pid {} is killed .....".format(pid))
        
    else:
        print("已經殺過嘞!!")
    
    



def rebootTheMachine():
    saveTime.recordTime("reboot") #被殺掉的時間
    os.system('reboot')
    print("reboot .....")

def disableNetworkInterface():
    saveTime.recordTime("reboot") #被殺掉的時間
    os.system('reboot')
    print("reboot .....")
    

def setTimeToKill(sec:int,this_output_path):
    from threading import Timer
    global killStatus_file

    killStatus_file = '{}/{}'.format(this_output_path,"killStatus.txt")
    print(killStatus_file)

    killStatus = False
    # 如果沒有狀態，創建 自殺計時器
    if not os.path.isfile(killStatus_file):
        np.savetxt(killStatus_file, ["True"], fmt="%s", delimiter=' ')
        killStatus = True
    else:
        killStatus = ("True"==np.loadtxt(killStatus_file,delimiter=' ', dtype=str))
    
    # 設定計時器並回傳
    ## 由主控端決定啟動與取消
    return killStatus, Timer(sec, killTheProcess)

    


# if "__main__"==__name__:
    
#     np.savetxt('./task.txt', ["True"], fmt="%s", delimiter=' ')
#     print(np.loadtxt('./task.txt',delimiter=' ', dtype=str))

#     train_loss_list = np.genfromtxt("{}/vali_Loss.csv".format("./"), dtype=str, delimiter =",").tolist()
#     print(train_loss_list)
   
import matplotlib.pyplot as plt
import  os

output_path=""

## 設定 預設儲存路徑
def checkPath(default_output_path:str ):
    
    global output_path

    output_path = default_output_path

    if not os.path.isdir(output_path):
       raise FileNotFoundError(output_path+"\n You need call setAndCheckPath function!")
    
    
    
def paint_line_chart(title, label, data_list,label_2, data_list_2):
    global output_path

    # 創建圖形和軸
    fig, ax1 = plt.subplots()

    # 設定標題
    plt.title(title)

    # 繪製準確度
    plt.plot(data_list, color='blue', marker='o', label=label)
    plt.plot(data_list_2, color='red', marker='o', label=label_2)
    ax1.set_xlabel('Epoch')
    ax1.set_ylabel(label)

    # 顯示圖例
    plt.tight_layout()
    plt.legend() # 显示图例

    # 判斷或建立路徑，以利Checkpoint file 的存檔
    if output_path == "":
        print("=================================")
        print("You need to use checkPath func!!!")
        print("=================================")
        raise ValueError("ValueError: Missing output_path value")

    plt.savefig('{path}/savefig_example_{title}.png'.format(path=output_path,title=title))

def paint_roc_curve():
    

    return plt
    
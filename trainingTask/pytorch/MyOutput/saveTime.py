# import argparse
import os
import numpy as np
from datetime import datetime

output_path=""
recordTimeListPath=""
recordTimeList=[]


## 設定 預設儲存路徑
def checkPathAndFile(default_output_path:str):
    '''
    設定 事件與時間的 .csv 檔案 的存檔位置
    input： 預計儲存位址
    '''
    global output_path, recordTimeListPath,recordTimeList
    
    output_path = default_output_path

    if not os.path.isdir(output_path):
       raise FileNotFoundError(output_path+"\n You need call setAndCheckPath function!")
    
        
    recordTimeListPath = "{}/recordStartTimeList.csv".format(output_path)

    # 讀取是否有檔案，有則將資料移入至 List Value 中
    if os.path.isfile(recordTimeListPath):
        recordTimeList = []
        tempList = np.genfromtxt(recordTimeListPath, dtype=str, delimiter = "," ).tolist() 
        print("===================")
        print("The task have a list.")
        print("tempList",tempList)
        print("===================")
        if len(tempList) == 0:
            recordTimeList = []
        else:
            if type(tempList[0]) == str:
                recordTimeList.append(tempList)
            else:
                recordTimeList=tempList
        # print(recordTimeList)
    else:
        print("===================")
        print("The task is new.")
        print("===================")
        
    

def recordTime(col:str):
    '''
    將此程式的事件與時間輸出為 .csv 檔案
    input： 紀錄事件的名稱
    Output： 當下時間：str, 事件的名稱:str
    '''
    global recordTimeListPath, recordTimeList
    
    # 判斷或建立路徑，以利Checkpoint file 的存檔
    if output_path == "":
        print("========================================")
        print("You need to use checkPathAndFile func!!!")
        print("========================================")
        raise ValueError("ValueError: Missing output_path value")
    # col: 事件名稱，如: start、closs
    date_time = datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3] # 列印到毫秒的時間
    recordTimeList.append([col, date_time]) # start time
    # print("recordTimeList",recordTimeList)
    np.savetxt(recordTimeListPath, recordTimeList, delimiter =",", fmt ='%s')

    print("==========================================")
    print(" the {}  event is at {}!!! ".format(col, date_time))
    print("==========================================")

    return date_time, col

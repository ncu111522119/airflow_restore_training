import os

def setAndCheckPath(pathList:list):
    '''
    檢查及創建 Path
    => Input(list): ["./The", "path", "you", "need"]
    => Output(str): Path
    '''
    
    output_path=""
    
    for i in range(len(pathList)):
        if i != 0:
            output_path = "{}/{}".format(output_path,pathList[i])
        else:
            output_path = pathList[i]

        if not os.path.isdir(output_path):
            os.mkdir(output_path)


    print("output_path:", output_path)
    
    return output_path

# if "__main__" == __name__:
#     setAndCheckPath(["./dataset", "output", "123"])